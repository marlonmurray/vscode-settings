# Setup Visual Studio Code Extensions and settings

### Install extensions
```bash
while read line; do code --install-extension "$line";done < vs-code-extensions.txt
```

